package servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database_sim.Database;
import database_sim.DrinkConverter;
import model.CocktailDB;

@WebServlet("/random")
public class Random extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Random() {
        super(); 
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		File file = new File(getServletContext().getRealPath("WEB-INF/database.db"));
		Database database = DrinkConverter.getDB(file);
		List<CocktailDB> cocktails = database.getCocktail();
		java.util.Random random = new java.util.Random();
		// Prende un numero casuale compreso tra 0 e la size dell'array - 1 
		request.setAttribute("cocktail", cocktails.get(random.nextInt(cocktails.size()-1)));
		request.getRequestDispatcher("/WEB-INF/jsp/random.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
