package servlet;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database_sim.Database;
import database_sim.DrinkConverter;
import model.CocktailDB;

@WebServlet("/giornaliero")
public class Giornaliero extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Giornaliero() {
        super();
    }
    
    protected int verifyInt(int length, int random) {
    	// Porcata assurda per poter generare un numero valido dall'elenco, tempo permettendo 
    	// Valuter� un metodo pi� efficiente e pulito
    	return random<length?random:verifyInt(length, random/2+1);
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		int random = calendar.get(Calendar.YEAR)+calendar.get(Calendar.MONTH)+calendar.get(Calendar.DAY_OF_MONTH);
		File file = new File(getServletContext().getRealPath("WEB-INF/database.db"));
		Database database = DrinkConverter.getDB(file);
		List<CocktailDB> cocktails = database.getCocktail();
		// Verifica che il numero random sia inferiore alla grandezza della lista
		random = verifyInt(cocktails.size(), random); 
		request.setAttribute("cocktail", cocktails.get(random));
		request.getRequestDispatcher("/WEB-INF/jsp/giornaliero.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
