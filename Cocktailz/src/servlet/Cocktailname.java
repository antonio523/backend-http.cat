package servlet;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database_sim.Database;
import database_sim.DrinkConverter;
import model.CocktailDB;

@WebServlet("/cocktailname")
public class Cocktailname extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Cocktailname() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/formricercacocktail.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		File file = new File(getServletContext().getRealPath("WEB-INF/database.db"));
		Database database = DrinkConverter.getDB(file);
		List<CocktailDB> cocktails = database.getCocktail();
		cocktails.removeIf(n -> (!n.getNome().contains(name)));
		request.setAttribute("cocktail", cocktails);
		request.getRequestDispatcher("/WEB-INF/jsp/founddrinkornot.jsp").forward(request, response);
	}

}
