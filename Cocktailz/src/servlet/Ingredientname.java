package servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database_sim.Database;
import database_sim.DrinkConverter;
import model.IngredienteDB;

@WebServlet("/ingredientname")
public class Ingredientname extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Ingredientname() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/formricercaingrediente.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		File file = new File(getServletContext().getRealPath("WEB-INF/database.db"));
		Database database = DrinkConverter.getDB(file);
		List<IngredienteDB> ingredienti = database.getIngredienti();
		// Ho rimosso la rimozione perch� l'array degli ingredienti � null :c
		//ingredienti.removeIf(n -> (!n.getNome().contains(name)));
		request.setAttribute("ingredienti", ingredienti);
		request.getRequestDispatcher("/WEB-INF/jsp/foundingredientornot.jsp").forward(request, response);
	}

}
