package model;

import java.io.Serializable;

public class Ingrediente implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	String idIngredient, strIngredient, strDescription, strType, strAlcohol, strABV;
	
	public Ingrediente() {
		super();
	}

	public String getIdIngredient() {
		return idIngredient;
	}

	public void setIdIngredient(String idIngredient) {
		this.idIngredient = idIngredient;
	}

	public String getStrIngredient() {
		return strIngredient;
	}

	public void setStrIngredient(String strIngredient) {
		this.strIngredient = strIngredient;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	public String getStrType() {
		return strType;
	}

	public void setStrType(String strType) {
		this.strType = strType;
	}

	public String getStrAlcohol() {
		return strAlcohol;
	}

	public void setStrAlcohol(String strAlcohol) {
		this.strAlcohol = strAlcohol;
	}

	public String getStrABV() {
		return strABV;
	}

	public void setStrABV(String strABV) {
		this.strABV = strABV;
	}
	
}
