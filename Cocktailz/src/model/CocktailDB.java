package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CocktailDB implements Serializable, Comparator<CocktailDB>{

	private static final long serialVersionUID = 1L;
	
	String nome;
	List<IngredienteDB> ingredienti;
	
	public CocktailDB(Cocktail cocktail) {
		this.nome = cocktail.getStrDrink();
		ingredienti = new ArrayList<>();
		if(cocktail.getStrIngredient1() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient1()));
		if(cocktail.getStrIngredient2() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient2()));
		if(cocktail.getStrIngredient3() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient3()));
		if(cocktail.getStrIngredient4() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient4()));
		if(cocktail.getStrIngredient5() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient5()));
		if(cocktail.getStrIngredient6() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient6()));
		if(cocktail.getStrIngredient7() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient7()));
		if(cocktail.getStrIngredient8() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient8()));
		if(cocktail.getStrIngredient9() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient9()));
		if(cocktail.getStrIngredient10() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient10()));
		if(cocktail.getStrIngredient11() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient11()));
		if(cocktail.getStrIngredient12() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient12()));
		if(cocktail.getStrIngredient13() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient13()));
		if(cocktail.getStrIngredient14() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient14()));
		if(cocktail.getStrIngredient15() != null) ingredienti.add(new IngredienteDB(cocktail.getStrIngredient15()));
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<IngredienteDB> getIngredienti() {
		return ingredienti;
	}

	public void setIngredienti(List<IngredienteDB> ingredienti) {
		this.ingredienti = ingredienti;
	}

	@Override
	public int compare(CocktailDB o1, CocktailDB o2) {
		return o1.getNome().compareTo(o2.getNome());
	}
	
	
}
