package model;

import java.io.Serializable;
import java.util.Comparator;

public class IngredienteDB implements Serializable, Comparator<IngredienteDB>{

	private static final long serialVersionUID = 1L;
	
	private String nome;
	
	public IngredienteDB(Ingrediente ingrediente) {
		this.nome = ingrediente.strIngredient;
	}
	
	public IngredienteDB(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int compare(IngredienteDB o1, IngredienteDB o2) {
		return o1.getNome().compareTo(o2.getNome());
	}


}
