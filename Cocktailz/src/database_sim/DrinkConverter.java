package database_sim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import model.Cocktail;
import model.CocktailDB;
import model.Ingrediente;
import model.IngredienteDB;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DrinkConverter {

	/** Lo scopo di questa classe � quello di convertire i vari cocktail presenti sul sito https://www.thecocktaildb.com/ in oggetti di tipo 
	 *   Cocktail e salvarli su un file per simulare un db
	 */
	
	public static void createDB() {
		String path = "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?iid=";
		final int lastID = 601;
		
		System.out.println("Mixing Ingredients...");
		
		// Converto gli ingredienti 
		
		List<IngredienteDB> ingredienti = new ArrayList<>();
		Request request = null;
		for(int i = 1; i <= lastID; i++) {
			if(i%100 == 0) try {
				System.out.print("1/6 +");
				Thread.sleep(1000);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			request = new Request.Builder()
					.url(path+i)
					.build();
			String requestResult = null;
			try {
				Response response = new OkHttpClient().newCall(request).execute();
				if(response.code() == 200) {
					requestResult = response.body().string();
					Ingrediente ingrediente = null;
					ingrediente = new Gson().fromJson(requestResult, Ingrediente.class); 
					ingredienti.add(new IngredienteDB(ingrediente));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Converto i cocktail
		
		System.out.println("\nShaking Cocktails...");
		
		List<CocktailDB> cocktails = new ArrayList<>();
		request = null;
		path = "https://www.thecocktaildb.com/api/json/v1/1/search.php?f=";
		int start = 'a'; int end = 'z';
		for(int i = start; i <= end; i++) {
			System.out.print("Cocktails with "+((char)i));
			request = new Request.Builder()
					.url(path+((char)i))
					.build();
			String requestResult = null;
			try {
				Response response = new OkHttpClient().newCall(request).execute();
				if(response.code() == 200) {
					requestResult = response.body().string();
					List<Cocktail> someCocktails = null;
					MiaRequest m = new Gson().fromJson(requestResult, MiaRequest.class);
					System.out.println(requestResult);
					someCocktails = m.getDrinks();
					List<CocktailDB> shackedCocktails = new ArrayList<>();
					if(someCocktails != null && someCocktails.size() > 0)
					for(Cocktail c : someCocktails) {
						shackedCocktails.add(new CocktailDB(c));
					}
					cocktails.addAll(shackedCocktails);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// Scrivo i cocktail sul file di testo che simuler� il db
		
		System.out.println("Serving the best drink...");
		
		Database database = new Database();
		database.setCocktail(cocktails);
		database.setIngredienti(ingredienti);
		
		try {
		FileOutputStream fos = new FileOutputStream(new File("database.db"));
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(database);
		oos.close();
		fos.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Enjoy and leave a review!");
	}
	
	public static Database getDB(File file) {
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			return (Database) ois.readObject();
			} catch(FileNotFoundException e) {
				e.printStackTrace();
				//createDB();
			} catch(Exception e) {
				e.printStackTrace();
			}
		return null;
	}
	
}
