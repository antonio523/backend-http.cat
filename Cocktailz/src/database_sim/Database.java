package database_sim;

import java.io.Serializable;
import java.util.List;

import model.CocktailDB;
import model.IngredienteDB;
import model.Utente;

public class Database implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<IngredienteDB> ingredienti;
	
	private List<CocktailDB> cocktail;
	
	private List<Utente> utente;
	
	public Database() {
		super();
	}

	public List<IngredienteDB> getIngredienti() {
		return ingredienti;
	}

	public void setIngredienti(List<IngredienteDB> ingredienti) {
		this.ingredienti = ingredienti;
	}

	public List<CocktailDB> getCocktail() {
		return cocktail;
	}

	public void setCocktail(List<CocktailDB> cocktail) {
		this.cocktail = cocktail;
	}

	public List<Utente> getUtente() {
		return utente;
	}

	public void setUtente(List<Utente> utente) {
		this.utente = utente;
	}
	
	@Override
	public String toString() {
		return ingredienti.toString()+cocktail.toString();
	}
	
}
