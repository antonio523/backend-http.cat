package database_sim;

import java.util.List;

import model.Cocktail;

public class MiaRequest {

	private List<Cocktail> drinks;
	
	public MiaRequest() {
		super();
	}

	public List<Cocktail> getDrinks() {
		return drinks;
	}

	public void setDrinks(List<Cocktail> drinks) {
		this.drinks = drinks;
	}
	
	
}
