<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="model.CocktailDB" %>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Digita il nome (o parte) dell'ingrediente da cercare!</title>
</head>
<body>
	<form action="ingredientname" method="POST">
  <label for="name">Nome Ingrediente:</label><br>
  <input type="text" id="name" name="name"><br>
  <input type="submit" value="Cerca l'ingrediente!">
</form>
</body>
</html>